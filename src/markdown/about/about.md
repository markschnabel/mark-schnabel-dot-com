---
bio: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi ea libero nihil repudiandae suscipit sit aliquam 
rerum et quaerat, architecto facere cupiditate eaque facilis quidem! Mollitia totam exercitationem recusandae id? Lorem 
ipsum dolor sit amet consectetur adipisicing elit.'
programmingLanguages: 'Python, Java, JavaScript, Node.js, C++, HTML, CSS, SQL'
libsAndFrameworks: 'Flask, Express, React, Redux, Gatsby, Tailwind, GraphQL, OpenCV, Numpy, JUnit'
toolsEnvsDatabases: 'Git, GitHub, Linux, PostgreSQL, MySQL, MongoDB, Postman, Eclipse, Visual Studio, IntelliJ'
---
