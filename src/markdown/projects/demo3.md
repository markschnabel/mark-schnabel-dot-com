---
title: 'Demo Project 3'
projectNumber: 3
description: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit.
  Asperiores at optio sed reprehenderit, porro magni iusto,
  cumque ab ea amet ipsa eveniet perferendis vitae totam? Vitae
  natus necessitatibus doloremque architecto?'
techStack: 'JavaScript, React, Gatsby, TailwindCSS, EmotionJS, Netlify'
projectLink: '#'
repoLink: '#'
image: '../../images/projects/demo.jpg'
---
