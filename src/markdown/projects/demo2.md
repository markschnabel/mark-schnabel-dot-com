---
title: 'Demo Project 2'
projectNumber: 2
description: 'Lorem ipsum, dolor sit amet consectetur adipisicing elit.
  Asperiores at optio sed reprehenderit, porro magni iusto,
  cumque ab ea amet ipsa eveniet perferendis vitae totam? Vitae
  natus necessitatibus doloremque architecto?'
techStack: 'JavaScript, React, Gatsby, TailwindCSS, EmotionJS, Netlify'
repoLink: '#'
image: '../../images/projects/demo.jpg'
---
